function Botes(elemento1, elemento2, elemento3, elemento4){
    this.elemento1 = elemento1;
    this.elemento2 = elemento2;
    this.elemento3 = elemento3;
    this.elemento4 = elemento4;
}
if(window.addEventListener){
    window.addEventListener("load", VerEstado, false);
}
else if(window.attachEvent){
    window.attachEvent("onload", VerEstado);
}
function VerEstado(){
    if(Botes.elemento1 == true){
        document.getElementById("elemento1").disabled = true;
    }
    else{
        document.getElementById("elemento1").disabled = false;
        Botes.elemento1 = false;
    }
    if(Botes.elemento2 == true){
        document.getElementById("elemento2").disabled = true;
    }
    else{
        document.getElementById("elemento2").disabled = false;
        Botes.elemento2 = false;
    }
    if(Botes.elemento3 == true){
        document.getElementById("elemento3").disabled = true;
    }
    else{
        document.getElementById("elemento3").disabled = false;
        Botes.elemento3 = false;
    }
    if(Botes.elemento4 == true){
        document.getElementById("elemento4").disabled = true;
    }
    else{
        document.getElementById("elemento4").disabled = false;
        Botes.elemento4 = false;
    }
    if(document.getElementById("elemento1").addEventListener){
        document.getElementById("elemento1").addEventListener("click", Alquilar1, false);
    }
    else if(document.getElementById("elemento1").attachEvent){
        document.getElementById("elemento1").attachEvent("onclick", Alquilar1);
    }
    if(document.getElementById("elemento2").addEventListener){
        document.getElementById("elemento2").addEventListener("click", Alquilar2, false);
    }
    else if(document.getElementById("elemento2").attachEvent){
        document.getElementById("elemento2").attachEvent("onclick", Alquilar2);
    }
    if(document.getElementById("elemento3").addEventListener){
        document.getElementById("elemento3").addEventListener("click", Alquilar3, false);
    }
    else if(document.getElementById("elemento3").attachEvent){
        document.getElementById("elemento3").attachEvent("onclick", Alquilar3);
    }
    if(document.getElementById("elemento4").addEventListener){
        document.getElementById("elemento4").addEventListener("click", Alquilar4, false);
    }
    else if(document.getElementById("elemento4").attachEvent){
        document.getElementById("elemento4").attachEvent("onclick", Alquilar4);
    }
}
function Alquilar1(){
    Botes.elemento1 = true;
    alert("Bote alquilado correctamente.");
    document.getElementById("elemento1").style.backgroundColor = '#256A21';
    document.getElementById("elemento1").innerHTML = "Reservado";
    VerEstado();
}
function Alquilar2(){
    Botes.elemento2 = true;
    alert("Bote alquilado correctamente.");
    document.getElementById("elemento2").style.backgroundColor = '#256A21';
    document.getElementById("elemento2").innerHTML = "Reservado";
    VerEstado();
}
function Alquilar3(){
    Botes.elemento3 = true;
    alert("Bote alquilado correctamente.");
    document.getElementById("elemento3").style.backgroundColor = '#256A21';
    document.getElementById("elemento3").innerHTML = "Reservado";
    VerEstado();
}
function Alquilar4(){
    Botes.elemento4 = true;
    alert("Bote alquilado correctamente.");
    document.getElementById("elemento4").style.backgroundColor = '#256A21';
    document.getElementById("elemento4").innerHTML = "Reservado";
    VerEstado();
}